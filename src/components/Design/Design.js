import React, {useState, useEffect} from 'react';
import './Design.css';
import { Navigation, Pagination } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/effect-coverflow';
import {designData} from '../../data/designData';

const Design = () => {
    const [slidesPerView, setSlidesPerView] = useState(3); // Initial value for desktop

    useEffect(() => {
      const handleResize = () => {
        setSlidesPerView(window.innerWidth > 1024 ? 3 : 1);
      };
  
      window.addEventListener('resize', handleResize);
      return () => {
        window.removeEventListener('resize', handleResize);
      };
    }, []);
  return (
    <div className="design-container">
        <h1 className="design-text">
            Get ideas to select your unique shades
        </h1>
        <Swiper
        grabCursor={true}
        centeredSlides={true}
        loop={true}
        slidesPerView={slidesPerView}
        pagination={{ el: '.swiper-pagination', clickable: true }}
        navigation={{
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
          clickable: true,
        }}
        modules={[Pagination, Navigation]}
        className="swiper_container"
      >
        {designData.map((design, index) => (
            <SwiperSlide>
            <img src={design.image} alt="slide_image" className="swiper-image"/>
          </SwiperSlide>
        ))}

        <div className="slider-controler">
          <div className="swiper-button-prev slider-arrow">
            <ion-icon name="arrow-back-outline"></ion-icon>
          </div>
          <div className="swiper-button-next slider-arrow">
            <ion-icon name="arrow-forward-outline"></ion-icon>
          </div>
        </div>
      </Swiper>
    </div>
  );
}

export default Design;