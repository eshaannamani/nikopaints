import React, { useState, useRef } from 'react';
import { Carousel, Image, Row, Col } from 'react-bootstrap';
import { FaArrowLeft, FaArrowRight } from 'react-icons/fa';
import { imageData } from "../../data/imageData";
import { colorData } from '../../data/colorData';
import './ColorSelector.css';
import { Slide } from 'react-toastify';


const ColorSelector = () => {
    const defaultColor = 'beige';
    const [selectedImage, setSelectedImage] = useState(null);
    const carouselRef = useRef(null);
    const [selectedColor, setSelectedColor] = useState('red');
    const [selectedShade, setSelectedShade] = useState(null);

    const handleImageClick = (id) => {
        setSelectedImage(id);
    };
    const handleNext = () => {
        carouselRef.current.next();
    };

    const handlePrev = () => {
        carouselRef.current.prev();
    };
    const handleColorClick = (color) => {
        setSelectedColor(color);
        setSelectedShade(null);
    };

    const handleShadeClick = (shade) => {
        setSelectedShade(shade);

    };
    return (
        <div className="color-selector" id="color-selector">
            <div className="picker" id="picker">
                <span className="pic-title">Visualize your selection</span>
            </div>
            <div className="color-selector-visualizer">
                <div className="carousel-two">
                    <div className="car-container">
                        <Carousel ref={carouselRef} prevIcon={null} nextIcon={null} interval={null} indicators={false}>
                            {imageData.map((data) => (
                                <Carousel.Item key={data.id} className='carousel-item-one'>
                                    <div
                                        style={{
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            height: '100%',
                                            width: '100%',

                                        }}
                                    >
                                        <Image
                                            src={data.image}
                                            alt={`Image ${data.id}`}
                                            onClick={() => handleImageClick(data.id)}
                                            style={{
                                                maxWidth: '100%', maxHeight: '100%', objectFit: 'contain',
                                                backgroundColor: selectedImage === data.id && selectedShade != null ? selectedShade.code : data.default,
                                                cursor: 'pointer',
                                            }}
                                        />
                                    </div>
                                </Carousel.Item>
                            ))}
                        </Carousel>

                    </div>
                    <div className="icon-container">
                        <FaArrowLeft className="icon-left" onClick={handlePrev} />
                        <FaArrowRight className="icon-right" onClick={handleNext} />
                    </div>
                </div>
                <div className="color-picker">
                    <div className="color-button">
                        {colorData.map((color, index) => (
                            <div key={index} className="color">
                                <label className='color-text'>
                                    <input
                                        type="radio"
                                        id={`color-${index}`}
                                        name="color"
                                        value={color.color}
                                        checked={selectedColor === color.color}
                                        onChange={() => handleColorClick(color.color)}
                                        style={{ backgroundColor: color.code }}
                                    />
                                    {color.color}
                                </label>
                            </div>
                        ))}
                    </div>
                    <div className="color-palette scrollable-div">
                        {selectedColor && (
                            <div className="shades">
                                {colorData
                                    .find((color) => color.color === selectedColor)
                                    .shades.map((shade, index) => (
                                        <button
                                            key={index}
                                            style={{ backgroundColor: shade.code, }}
                                            onClick={() => handleShadeClick(shade)}
                                            className='palete-btn'
                                        >
                                        </button>
                                    ))}
                            </div>
                        )}
                    </div>
                </div>
            </div>

        </div>
    )
}

export default ColorSelector






