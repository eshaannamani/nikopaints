import React from 'react';
import { FaPalette, FaPaintBrush, FaFillDrip, FaShieldAlt, FaClock, FaSun } from 'react-icons/fa';
import './WhyChooseUs.css';
import Gimage from "../../assets/chooseus/grid1.png"

const WhyChooseUs = () => {
    return (
        <section className="why-choose-us" id="choose-us">
            <div className="choose"><span className='grid-title'>Why Choose Us?</span></div>
            <div className="container1">
                <div className='image1'><img src={Gimage} alt="Grid image" /></div>
                <div className='feature1'>
                <div className="feature">
                <FaPalette className='icon1'/><h3>Exclusive Paint Collections</h3>
                </div>
                    <div className="feature">
                    <FaPaintBrush className='icon1'/><h3>Paint Supplies Hub</h3>
                    </div>
                    <div className="feature">
                    <FaFillDrip className='icon1'/><h3>Quick Custom Colors</h3>
                    </div>
                    <div className="feature">
                    <FaShieldAlt className='icon1'/><h3>Premium Durability</h3>
                    </div>
                    <div className="feature">
                    <FaSun className='icon1'/> <h3>Sun and Rain Protection</h3>
                    </div>
                    <div className="feature">
                    <FaClock className='icon1' /><h3>Long-lasting Quality</h3>
                    </div></div>
            </div>
        </section>
    );
};

export default WhyChooseUs;
