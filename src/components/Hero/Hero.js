import React from "react";
import Carousel from "react-bootstrap/Carousel";
import { heroData } from "../../data/heroData";
import "./Hero.css";
import iso3 from "../../assets/hero/iso3.png";
import iso2 from "../../assets/hero/iso2.png";
import NumberCounter from "number-counter";
import { motion } from "framer-motion";

const Hero = () => {
  const transition = { type: "spring", duration: 3 };
  const mobile = window.innerWidth <= 769 ? true : false;
  return (
    
    <section className="hero-block" id="hero">
      <Carousel data-bs-theme="dark" pause={false} interval={5000}>
        <Carousel.Item key={heroData[0].id}>
          <img
            className="d-block w-100 hero-img"
            src={heroData[0].image}
            alt="First slide"
          />
          <div className="custom-captions">
            <div className="custom-caption-1-1">
              <h3 className="hero-title">Niko Paints</h3>
              <img className="hero-iso-1" src={iso3} alt="caption" />
            </div>
            <div className="custom-caption-1-2">
              <motion.div
                initial={{ left: mobile ? "100px" : "238px" }}
                whileInView={{ left: "8px" }}
                transition={{ ...transition, type: "tween" }}
              ></motion.div>
              <span className="hero-caption-1">{heroData[0].caption}</span>
            </div>
          </div>
        </Carousel.Item>
        <Carousel.Item key={heroData[1].id}>
          <img
            className="d-block w-100 hero-img"
            src={heroData[1].image}
            alt="Second slide"
          />
          <div className="custom-captions">
            <div className="custom-caption-2-1">
              <h3 className="hero-title">Niko Paints</h3>
              <img className="hero-iso-2" src={iso2} alt="caption" />
            </div>
            <div className="custom-caption-2-3">
              <div className="figOne">
                <span>
                  <NumberCounter end={2000} start={1000} delay="4" preFix="+" />
                </span>
                 <div>
                    <span>Trusted Hearts</span>
                 </div>
              </div>
              <div className="figTwo">
                <div>
                  <span>
                    <NumberCounter end={20} start={1} delay="6" preFix="+" />
                  </span>
                  <div>
                    <span>Quality Products</span>
                  </div>
                </div>
                <div>
                  <span>
                    <NumberCounter end={25} start={1} delay="6" preFix="+" />
                  </span>
                  <div>
                    <span>Years Of Perfection</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Carousel.Item>
        <Carousel.Item key={heroData[2].id}>
          <img
            className="d-block w-100 hero-img"
            src={heroData[2].image}
            alt="Third slide"
          />
        </Carousel.Item>
      </Carousel>
    </section>
  );
};

export default Hero;
