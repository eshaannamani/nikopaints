import React from "react";
import "./Other.css";
import { otherData } from "../../data/otherData";
import { IoCall, IoMail, IoLocation, IoLogoWhatsapp } from "react-icons/io5";

const Other = () => {
  const handlePhoneClick1 = () => {
    window.open("tel:9791021666");
  };

  const handlePhoneClick2 = () => {
    window.open("tel:9865908902");
  };

  const handleEmailClick = () => {
    window.open("mailto:nikopaints@gmail.com");
  };

  const handleLocationClick = () => {
    window.open(
      "https://www.google.com/maps/place/Niko+Paints/@13.0554903,80.1358443,18z/data=!4m6!3m5!1s0x3a5261005b088759:0x4665d1692f8de53e!8m2!3d13.0543697!4d80.137838!16s%2Fg%2F11k0m1ssfk?entry=ttu"
    );
  };
  const handleWhatsappClick = () => {
    const phoneNumber = "9791021666";
    const webUrl = `https://web.whatsapp.com/send?phone=${phoneNumber}`;
    window.open(webUrl, "_blank");
  };
  return (
    <div className="other-container" id="footer">
      <span className="other-title">All other available brands</span>
      <div className="other-img">
        {otherData.map((other) => (
          <img src={other.image} alt="other data" />
        ))}
      </div>
      <footer>
        <div className="footer-container">
          <div className="contact">
            <div className="footer-icon">
              <div className="icon-circle">
                <IoCall className="icon" />
              </div>
            </div>
            <div className="footer-text">
              <span onClick={handlePhoneClick1}>+919791021666</span>
              <span onClick={handlePhoneClick2}>+919865908902</span>
            </div>
          </div>
          <div className="whatsapp">
            <div className="footer-icon">
              <div className="icon-circle">
                <IoLogoWhatsapp className="icon" />
              </div>
            </div>
            <div className="footer-text" onClick={handleLocationClick}>
              <span onClick={handleWhatsappClick}>+919791021666</span>
            </div>
          </div>
          <div className="email">
            <div className="footer-icon">
              <div className="icon-circle">
                {" "}
                <IoMail className="icon" />
              </div>
            </div>
            <div className="footer-text">
              <span onClick={handleEmailClick}>nikopaints@gmail.com</span>
            </div>
          </div>
          <div className="location">
            <div className="footer-icon">
              <div className="icon-circle">
                <IoLocation className="icon" />
              </div>
            </div>
            <div className="footer-text" onClick={handleLocationClick}>
              <span>No 12, Amirtham Nagar,</span>
              <span>Noombal Road,</span>{" "}
              <span>Velappanchavadi, Chennai-600037</span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
};

export default Other;
