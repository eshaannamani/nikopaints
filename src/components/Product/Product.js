import React, { useState } from "react";
import "./Product.css";
import { productData } from "../../data/productData";
import CardFlip from "react-card-flip";

const Product = () => {
  const [isFlipped, setIsFlipped] = useState(
    Array(productData.length).fill(false)
  );

  const handleCardFlip = (index) => {
    setIsFlipped((prevFlips) => {
      const newFlips = [...prevFlips];
      newFlips[index] = !newFlips[index];
      return newFlips;
    });
  };

  return (
    <div className="d-inline" id="product">
      <div className="product">
        <span className="prod-title">products we deliver</span>
      </div>
      <div className="product-container">
        {productData.map((product, index) => (
          <CardFlip key={product.id} isFlipped={isFlipped[index]} flipDirection="horizontal"
          >
            <div className="product-card front" onClick={() => handleCardFlip(index)}>
              <img src={product.image} alt={product.title} className="prod-img"/>
              <span className="prod-link">Click to know more</span>
            </div>
            <div className="product-card back" onClick={() => handleCardFlip(index)}>
              <h3>{product.title}</h3>
              <hr />
              <p>{product.caption}</p>
              <a href={product.pdf} target="_blank" rel="noreferrer"><button className="prod-button">Know More</button></a>
            </div>
          </CardFlip>
        ))}
      </div>
    </div>
  );
};

export default Product;
