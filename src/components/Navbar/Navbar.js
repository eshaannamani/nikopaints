import React, {useState} from "react";
import "./Navbar.css";
import Logo from "../../assets/logo.png";
import MenuIcon from "@mui/icons-material/Menu";
import CloseIcon from "@mui/icons-material/Close";
const Navbar = () => {
    const [clicked, setClicked]=useState(false);
  return (
    <nav className="nav1">
      <div className="logo">
        <img src={Logo} alt="logo" className="logo-img"></img>
        <div className="title">Niko Paints</div>
      </div>
      <div>
        <ul id="navbar" className={clicked ? "#navbar active" : "navbar"}>
          <li>
            <a href="#product" onClick={() => setClicked(!clicked)}>Products</a>
          </li>
          <li>
            <a href="#service" onClick={() => setClicked(!clicked)}>Services</a>
          </li>
          <li>
            <a href="#choose-us" onClick={() => setClicked(!clicked)}>Why Us</a>
          </li>
          <li>
            <a href="#footer" onClick={() => setClicked(!clicked)}>Contact</a>
          </li>
        </ul>
      </div>
      <div id="mobile" onClick={() => setClicked(!clicked)}>
        {clicked ? (
          <CloseIcon className="close-icon" />
        ) : (
          <MenuIcon className="menu-icon" />
        )}
      </div>
    </nav>
  );
};

export default Navbar;
