import React from 'react'
import "./Aboutus.css"
const Aboutus = () => {
  return (
    <div className="about-us" id="about-us">
        <span>Niko Paints, a household name among millions today, was founded 25 years ago by the visionary and dynamic entrepreneur, Mr. M. Karthik Prabhu. Starting his career as a small paint trader, he demonstrated courage and decisive decision-making abilities. With a grand vision of coloring the world and bringing nature into people's homes, Karthik Prabhu's dream product, Niko Paints, has become a reality. As one of India's fastest-growing paint companies, Niko Paints now offers more than 15 products, each reflecting systematic and modern production and business management.</span>
        <span>Notably, Niko Paints is an ISO certified company, a testament to its unwavering commitment to quality and customer satisfaction. By adorning the walls of homes, Niko Paints has managed to touch the hearts of people and earn praise for its exceptional quality, prompt delivery, and outstanding service.</span>
    </div>
  )
}

export default Aboutus