import React, { useRef } from 'react'
import emailjs from "@emailjs/browser";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './Join.css';
const Join = () => {
  const form = useRef();
  const sendEmail = (e) => {
    e.preventDefault();
    emailjs
      .sendForm(
        "service_btfi7br",
        "template_m8swxsz",
        form.current,
        "dkj9nidCDT-drPdKj"
      )
      .then(
        (result) => {
          toast.success('Subscribed succesfully! Let us color your dream house!', { position: toast.POSITION.TOP_CENTER });
          form.current.reset();
        },
        (error) => {
          toast.error('Subscription Failure!', { position: toast.POSITION.TOP_CENTER });
        }
      );
  };
  return (
    <div className="join" id="join">
      <div className="join-text">
        <span>Are you ready to </span>
        <span>color your dream home</span>
        <span> with us?</span>
      </div>
    <div className="form-container">
      <form ref={form} className="email-container" onSubmit={sendEmail}>
        <input type="text" id="name" name="name" placeholder="Enter Your Name" required/><br/>
        <input type="tel" id="phone" name="phone" placeholder="Enter Your Phone number" required/><br/>
        <input type="email" name="user_email" placeholder="Enter Your Email Address"/><br/>
        <button className="btn-email">Join us</button>
       </form>
       <ToastContainer />
    </div>
    </div>
    )
}

export default Join