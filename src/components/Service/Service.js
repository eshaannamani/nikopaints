import React from 'react';
import './Service.css';

const Service = () => {
    return (
        <div className="service-section" id="service">
            <div className="service-title">
                <span>Services we offer</span>
            </div>
            <div className='service-con'>
                <div className="service-container">
                    <div className="service">
                        <span>Painting</span>
                        <span>Contracts</span>
                    </div>
                    <div className="service">
                        <span>Construction</span>
                        <span>of Building</span>
                    </div>
                    <div className="service">
                        <span>Material</span>
                        <span>Supply</span>
                    </div>
                    <div className="service">
                        <span>Color</span>
                        <span>Consultation</span>
                    </div>
                </div>
                <div className="text-container">
                    <span>Whether it's residential or industrial construction, we are your trusted partner, offering a wide range of products and services to help you achieve your construction goals with confidence.</span>
                </div>
                <div className="product1-container">
                    <div className="product1">
                        <span>Interior and</span>
                        <span>Exterior Rollers</span>
                    </div>
                    <div className="product1">
                        <span>All Paint </span>
                        <span>Products</span>
                    </div>
                    <div className="product1">
                        <span>Tiles of All </span>
                        <span>Leading Brands</span>
                    </div>
                    <div className="product1">
                        <span>Cements</span>
                    </div>
                    <div className="product1">
                        <span>Batteries and</span>
                        <span>Solar System</span>
                    </div>
                    <div className="product1">
                        <span>SYSKA</span>
                        <span>LED Products</span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Service;
