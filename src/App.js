import './App.css';
import Navbar from "./components/Navbar/Navbar";
import Hero from "./components/Hero/Hero";
import Aboutus from './components/Aboutus/Aboutus';
import Product from './components/Product/Product';
import WhyChooseUs from './components/WhyChooseUs/WhyChooseUs';
import Service from './components/Service/Service';
import Join from './components/Join/Join';
import 'bootstrap/dist/css/bootstrap.min.css';
import ColorSelector from './components/ColorSelector/ColorSelector';
import Design from './components/Design/Design';
import Other from './components/Other/Other';
function App() {
  return (
    <div className="App">
      <Navbar />
      <Hero />
      <Aboutus />
      <Product />  
      <WhyChooseUs /> 
      <ColorSelector />
      <Service/> 
      <Design />
      <Join />
      <Other />
    </div>
  );
}

export default App;
