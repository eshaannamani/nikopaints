import pimg1 from "../assets/colorpicker/pimg1.png";
import pimg3 from "../assets/colorpicker/pimg3.png";
import pimg4 from "../assets/colorpicker/pimg4.png";
import pimg5 from "../assets/colorpicker/pimg5.png";
import pimg6 from "../assets/colorpicker/pimg6.png";

export const imageData = [
    {
        id:1,   
        image: pimg1,
        default: '#f5f5dc',
    },
    {
        id:2,
        image:pimg3,
        default: '#ebebeb',
    },
    {
        id:3,   
        image: pimg4,
        default: '#fffafa',
    },
    {
        id:4,
        image: pimg5,
        default: '#d6fed6',
    },
    {
        id:5,
        image: pimg6,
        default: '#ffebeb',
    },

];