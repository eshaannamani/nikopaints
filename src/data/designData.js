import image1 from "../assets/design/bedroom1.jpg";
import image2 from "../assets/design/kidsroom1.jpg";
import image3 from "../assets/design/image1.jpg";
import image4 from "../assets/design/elegant2.jpg";
import image5 from "../assets/design/image12.jpg";
import image6 from "../assets/design/image6.jpg";
import image7 from "../assets/design/image7.jpg";
import image9 from "../assets/design/image8.jpg";
import image12 from "../assets/design/image3.jpg";
import image13 from "../assets/design/image11.jpg";
import image14 from "../assets/design/image4.jpg";
import image20 from "../assets/design/image15.jpg";
import image21 from "../assets/design/image14.jpg";


export const designData = [
    {
        id:1,   
        image: image1,
    },
    {
        id:2,
        image:image2,
    },
    {
        id:3,   
        image: image3,
    },
    {
        id:4,
        image: image4,
    },
    {
        id:5,
        image: image5,       
    },
    {
        id:6,   
        image: image6,
    },
    {
        id:7,
        image:image7,
    },
    {
        id:9,
        image: image9,
    },
    {
        id:12,
        image: image12,
    },
    {
        id:13,
        image:image13,
    },
    {
        id:14,   
        image: image14,
    },
    {
        id:20,   
        image: image20,
    },
    {
        id:21,   
        image: image21,
    },

];