import putty from "../assets/product/putty.jpg";
import emulsion from "../assets/product/emulsion.png";
import primer from "../assets/product/primer.png";
import primerpdf from "../assets/product/primer.pdf";
import puttypdf from "../assets/product/putty.pdf";
import emulsionpdf from "../assets/product/emulsion.pdf";
export const productData = [
    {
        id:1,
        title: "INTERIOR & EXTERIOR CEMENT PRIMER",
        caption: "Achieve flawless paint results with our versatile Interior & Exterior Primer. Specially designed for both interior and exterior surfaces, this high-performance primer creates a smooth and even base, enhancing the adhesion and durability of your paint. Its advanced formulation seals porous surfaces, blocks stains, and promotes long-lasting color vibrancy. Trust our primer to bring out the true beauty of your walls, both inside and outside your home." , 
        image:primer,
        pdf: primerpdf,
    },
    {
        id:2,
        title: "WALL CARE PUTTY",
        caption: "Transform your walls into a flawless canvas with our Wall Care Putty. This high-quality product smoothens rough surfaces, conceals imperfections, and creates a perfect base for painting. Its superior adhesion and durability ensure long-lasting results. Experience the magic of a seamless finish that adds elegance and charm to your living spaces. From rugged surfaces to pristine perfection, let your walls be the embodiment of sophistication. " , 
        image: putty,
        pdf: puttypdf,
    },
    {
        id:3, 
        title: "INTERIOR & EXTERIOR ACRYLIC EMULSION",
        caption: "Experience the perfect blend of beauty and protection with our Interior & Exterior Emulsion. This premium paint offers exceptional coverage, excellent color retention, and long-lasting durability for both interior and exterior surfaces. Its smooth application and quick-drying formula ensure a seamless finish, while its resistance to weathering and stains keeps your walls looking fresh and vibrant for years to come. Transform your living spaces with the ultimate combination of style and performance." , 
        image: emulsion,
        pdf: emulsionpdf,
    },

];