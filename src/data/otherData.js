import img1 from "../assets/other/dalmia.jpg";
import img2 from "../assets/other/dulux.jpg";
import img3 from "../assets/other/ica.jpg";
import img4 from "../assets/other/kag.jpg";
import img5 from "../assets/other/kajaria.jpg";
import img6 from "../assets/other/livfast.jpg";
import img7 from "../assets/other/luminous.jpg";
import img8 from "../assets/other/nerolac.jpg";
import img9 from "../assets/other/nippon.jpg";
import img10 from "../assets/other/syska.jpg";

export const otherData = [
    {
        id:1,   
        image: img1,
    },
    {
        id:2,
        image:img2,
    },
    {
        id:3,   
        image: img3,
    },
    {
        id:4,
        image: img4,
    },
    {
        id:5,
        image: img5,
    },
    {
        id:6,   
        image: img6,
    },
    {
        id:7,
        image:img7,
    },
    {
        id:8,   
        image: img8,
    },
    {
        id:9,
        image: img9,
    },
    {
        id:10,
        image: img10,
    },

];